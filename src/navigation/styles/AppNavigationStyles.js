import { StyleSheet } from 'react-native';

export default StyleSheet.create({
	headerStyle: {
		backgroundColor: '#3F51B5'
	},
	headerTitleStyle: {
		fontWeight: 'bold'
	},
	headerContainerTitle: {
		marginLeft: 10
	},
	headerTitle: {
		fontWeight: 'bold',
		fontSize: 18,
		color: '#fff'
	},
	tabStyle: {
		borderTopWidth: 1,
		borderTopColor: '#ececec',
		flex: 1
	},
	labStyle: {
		fontWeight: 'bold',
		fontSize: 12,
		color: '#c1c1c1',
		paddingBottom: 3
	},
	indicatorStyle: {
		backgroundColor: 'transparent'	
	}
});
