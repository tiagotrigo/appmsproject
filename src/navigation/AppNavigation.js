import React, { Component } from 'react';

import { 
	StackNavigator, 
	TabNavigator 
} from 'react-navigation';

import { 
	Platform, 
	Text, 
	View 
} from 'react-native';

import LoginScreen from '../screens/LoginScreen';
import AuthScreen from '../screens/AuthScreen';
import RadarScreen from '../screens/RadarScreen';
import ListScreen from '../screens/ListScreen';
import ProfileScreen from '../screens/ProfileScreen';
// Styles
import styles from './styles/AppNavigationStyles';

const TabNav = TabNavigator(
	{
		RadarScreenTab: {
			screen: RadarScreen
		},
		ListScreenTab: {
			screen: ListScreen
		},
		ProfileScreenTab: {
			screen: ProfileScreen
		}
	},
	{
		tabBarPosition: 'bottom',
		animationEnabled: false,
		swipeEnabled: false,
		initialRouteName: 'RadarScreenTab',
		tabBarOptions: {
			showLabel: true,
			showIcon: true,
			style: {
				backgroundColor: '#fff'
			},
			tabStyle: styles.tabStyle,
			labelStyle: styles.labelStyle,
			indicatorStyle: styles.indicatorStyle
		}
	}
);

const contentNavigator = StackNavigator(
	{
		TabRoot: { screen: TabNav },
		Loading: { screen: AuthScreen  },
		Login: { screen: LoginScreen }
	},
	{
		headerMode: Platform.OS === 'ios' ? 'float' : 'screen',
		initialRouteName: 'Login',
		navigationOptions: {
			headerStyle: styles.headerStyle,
			headerTitleStyle: styles.headerTitleStyle,
			headerTitle: (
				<View style={styles.headerContainerTitle}>
					<Text style={styles.headerTitle}>{'Tela do teste'}</Text>
				</View>
			)
		}
	}
);

export const AppNavigation = StackNavigator(
	{
		content: { screen: contentNavigator }
	},
	{
		headerMode: 'none',
		initialRouteName: 'content'
	}
);


export default AppNavigation;
