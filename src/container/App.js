import React, { Component } from 'react';
import RootContainer from './RootContainer';

/**
 * Provides an entry point into our application.  Both index.ios.js and index.android.js
 * call this component first.
 *
 * We create our Apollo client here, put it into the Apollo Provider and then bring in our
 * RootContainer.
 *
 * We separate like this to play nice with React Native's hot reloading.
 */
class App extends Component {
	render() {
		return <RootContainer />;
	}
}

// allow reactotron overlay for fast design in dev mode
export default App;
