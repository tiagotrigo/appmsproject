import React, { Component } from 'react';
import {
	View,
	StatusBar,
	Platform,
	BackHandler,
	NetInfo,
	Alert,
	DeviceEventEmitter,
	PermissionsAndroid
} from 'react-native';

import AppNavigation from '../navigation/AppNavigation';
import NavigationService from '../services/NavigationService';

import LocationServicesDialogBox from "react-native-android-location-services-dialog-box";

import styles from './styles/RootContainerStyles';

class RootContainer extends Component {
	
	constructor(props) {
		super(props);

		this.state = {
			isConnected: true
		};		
	}

	componentDidMount() {
		//process to disable the  hardware back button of android
		if (Platform.OS != 'ios') {
			BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
			this.requestLocationPermission();
			this.handleChangeGPS();
		}

		NetInfo.isConnected.addEventListener('connectionChange', this.handleConnectivityChange);

        DeviceEventEmitter.addListener('locationProviderStatusChange', (status) => {
            if (status.enabled === false ){
            	this.handleChangeGPS();
            }
        });
	}

	componentWillUnmount() {
		//process to disable the  hardware back button of android
		if (Platform.OS != 'ios') {
			BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
		}

		NetInfo.isConnected.removeEventListener('connectionChange', this.handleConnectivityChange);

		LocationServicesDialogBox.stopListener();
	}

	handleConnectivityChange = (isConnected) => {
		if (isConnected) {
			this.setState({ isConnected });
		} else {
			this.setState({ isConnected });
		}
	};

	handleChangeGPS = () => {
		LocationServicesDialogBox.checkLocationServicesIsEnabled({
            message: "Deseja habilitar o GPS ?",
            ok: "Sim",
            cancel: "Não",
            enableHighAccuracy: true,
            showDialog: true,
            openLocationServices: true,
            preventOutSideTouch: false,
            preventBackClick: false,
            providerListener: true
        }).catch((error) => console.log);
	};

	handleBackButton() {
		return true;
	}

	requestLocationPermission = async () => {
		try {
			const granted = await PermissionsAndroid.request(
				PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
				{
					title: 'Location Permission',
					message: 'Location Permission' + 'so you can use GPS location.'
				}
			);
			if (granted === PermissionsAndroid.RESULTS.GRANTED) {
				console.log('You can use GPS');
			} else {
				console.log('GPS location denied');
			}
		} catch (err) {
			console.warn(err);
		}
	};

	_handleAlert = (title, description) => {
		Alert.alert(
		  title,
		  description,
		  [
		    {
		      text: 'Cancelar',
		      style: 'cancel',
		    },
		  ],
		  {	cancelable: false },
		);
	};

	render() {
		const {
			isConnected
		} = this.state;
		
		if (!isConnected) {
			this._handleAlert('Conexão', 'Sem conexão, por favor habilitar.');
		}
		
		return (
			<View style={styles.applicationView}>
				<StatusBar barStyle="light-content" />

				<AppNavigation
					ref={navigatorRef =>
						NavigationService.setTopLevelNavigator(navigatorRef)
					}
				/>
			</View>
		);
	}
}

export default RootContainer;
