import { StyleSheet } from 'react-native';
import { metrics } from '../../themes';

export default StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: '#fff'
	},
	fbProfile: {
		flex: 1,
		alignItems: 'center',
		flexDirection: 'column',
		justifyContent: 'center'
	},
	loading: {
		backgroundColor: '#fff',
		flex: 1,
		alignItems: 'center',
		flexDirection: 'row',
		justifyContent: 'center'
	},
	iconNavBar: {
		height: 24,
		width: 24
	},
	navBarContainerLeft: {
		marginLeft: 16
	},
	navBarContainerRight: {
		marginRight: 16
	},
	navBarContainerTitle: {
		marginTop: 3
	},
	navBarTitle: {
		textAlign: 'center',
		fontSize: 10
	},
	fbPhoto: {
		borderRadius: 216 / 2,
		width: 216,
		height: 216
	},
	fbPhotoButton: {
		borderWidth: 4,
		borderColor: '#3F51B5',
		borderRadius: 224 / 2,
		backgroundColor: '#3F51B5',
		width: 224,
		height: 224,
		marginBottom: 30,
		alignItems: 'center',
		flexDirection: 'row',
		justifyContent: 'center',
		shadowOffset: {
	      width: 0,
	      height: 2
	    },
	    elevation: 2,
	    shadowRadius: 1,
	    shadowColor: '#000',
	    shadowOpacity: 0.13
	},
	fbName: {
		textAlign: 'center',
		fontWeight: '600',
		fontSize: 24,
		color: '#404040'
	},
	fbModalOverlay: {
		backgroundColor: 'rgba(0, 0, 0, .34)',
		flex: 1
	},
	fbModalLogout: {
		backgroundColor: '#fff',
		width: metrics.screenWidth,
		padding: 16,
		alignItems: 'flex-start',
		flexDirection: 'column',
		justifyContent: 'flex-start'
	},
	fbModalHeader: {
		borderBottomWidth: 1,
		borderBottomColor: '#f1f1f1',
		width: metrics.screenWidth,
		paddingBottom: 16,
		alignItems: 'center',
		flexDirection: 'row',
		justifyContent: 'flex-start'
	},
	fbModalHeaderText: {
		textAlign: 'center',
		fontWeight: '600',
		fontSize: 24,
		color: '#404040'
	},
	fbModalBody: {
		width: metrics.screenWidth - 32,
		paddingTop: 16,
		paddingBottom: 16,
		alignItems: 'flex-start',
		flexDirection: 'column',
		justifyContent: 'flex-start'
	},
	fbModalFooter: {
		width: metrics.screenWidth - 32,
		alignItems: 'center',
		flexDirection: 'row',
		justifyContent: 'center'
	},
	fbModalButton: {
		borderRadius: 4,
		flex: 1,
		height: 45,
		alignItems: 'center',
		flexDirection: 'row',
		justifyContent: 'center'
	},
	fbModalButtonCancel: {
		borderWidth: 2,
		borderColor: '#404040',
		marginRight: 10,
	},
	fbModalButtonExit: {
		backgroundColor: '#3F51B5'
	},
	fbModalButtonLabel: {
		textAlign: 'center',
		fontWeight: '600',
		fontSize: 14
	},
	fbModalButtonLabelCancel: {
		color: '#404040'
	},
	fbModalButtonLabelExit: {
		color: '#ffffff'
	}
});
