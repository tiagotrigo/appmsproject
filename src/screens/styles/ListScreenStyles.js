import { StyleSheet } from 'react-native';

export default StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: '#fff'
	},
	loading: {
		flex: 1,
		alignItems: 'center',
		flexDirection: 'row',
		justifyContent: 'center'
	},
	iconNavBar: {
		height: 24,
		width: 24
	},
	navBarContainerLeft: {
		marginLeft: 16
	},
	navBarContainerRight: {
		marginRight: 16
	},
	navBarContainerTitle: {
		marginTop: 3
	},
	navBarTitle: {
		textAlign: 'center',
		fontSize: 10
	},
	pItem: {
		borderBottomWidth: 1,
		borderBottomColor: '#ccc',
		flex: 1,
		padding: 16,
		alignItems: 'flex-start',
		flexDirection: 'row',
		justifyContent: 'flex-start'
	},
	pGroup: {
		flex: 1,
		alignItems: 'flex-start',
		flexDirection: 'column',
		justifyContent: 'flex-start'
	},
	pIcon: {
		borderWidth: 1,
		borderColor: '#ccc',
		borderRadius: 40 / 2,
		width: 40,
		height: 40,
		marginRight: 10,
		alignItems: 'center',
		flexDirection: 'row',
		justifyContent: 'center'
	},
	pThumb: {
		width: 24,
		height: 24
	},
	pName: {
		fontSize: 16,
		color: '#000000'
	},
	pAddressWrapper: {
		flex: 1,
		marginBottom: 8
	},
	pAddress: {
		fontSize: 14,
		color: '#969696',
		lineHeight: 18
	},
	pPhone: {
		fontWeight: 'bold',
		fontSize: 16,
		color: '#F19900'
	}
});
