import { StyleSheet } from 'react-native';
import { metrics } from '../../themes';

export default StyleSheet.create({
	container: {
		flex: 1
	},
	loading: {
		flex: 1,
		alignItems: 'center',
		flexDirection: 'row',
		justifyContent: 'center'
	},
	iconNavBar: {
		height: 24,
		width: 24
	},
	navBarContainerLeft: {
		marginLeft: 16
	},
	navBarContainerRight: {
		marginRight: 16
	},
	navBarContainerTitle: {
		marginTop: 3
	},
	navBarTitle: {
		textAlign: 'center',
		fontSize: 10
	},
	fbProfile: {
		borderRadius: 75 / 2,
		borderWidth: 4,
		borderColor: '#3F51B5',
		backgroundColor: '#3F51B5',
		width: 75,
		height: 75,
		overflow: 'hidden',
		alignItems: 'center',
		flexDirection: 'row',
		justifyContent: 'center'
	},
	fbPhoto: {
		borderRadius: 75 / 2,
		width: 75,
		height: 75
	},
	modal: {
		backgroundColor: 'rgba(0, 0, 0, .38)',
		flex: 1,
		padding: 32,
		alignItems: 'center',
		flexDirection: 'row',
		justifyContent: 'center'
	},
	modalHeader: {
		paddingBottom: 16,
		alignItems: 'center',
		flexDirection: 'row',
		justifyContent: 'flex-start'
	},
	modalHeaderTitle: {
		fontSize: 21,
		fontWeight: 'bold',
		color: '#969696'
	},
	modalContent: {
		backgroundColor: '#fff',
		width: metrics.screenWidth - 32,
		padding: 32,
		alignItems: 'flex-start',
		flexDirection: 'column',
		justifyContent: 'flex-start'
	},
	routeModalName: {
		fontSize: 26,
		fontWeight: 'bold',
		color: '#000'
	},
	routeModalVicinityWrapper: {
		paddingTop: 16,
		paddingBottom: 30
	},
	routeModalVicinity: {
		fontSize: 14,
		color: '#969696',
		lineHeight: 18
	},
	button: {
		backgroundColor: '#3F51B5',
		width: metrics.screenWidth - 96,
		height: 54,
		alignItems: 'center',
		flexDirection: 'row',
		justifyContent: 'center'
	},
	buttonLink: {
		width: metrics.screenWidth - 96,
		marginTop: 16,
		alignItems: 'center',
		flexDirection: 'row',
		justifyContent: 'flex-end'	
	},
	buttonLabel: {
		fontSize: 18,
		fontWeight: 'bold',
		color: '#fff'
	},
	buttonLinkLabel: {
		fontSize: 18,
		fontWeight: 'bold',
		color: '#3F51B5'
	},
	buttonCategory: {
		width: metrics.screenWidth - 96,
		height: 38,
		marginBottom: 8,
		alignItems: 'center',
		flexDirection: 'row',
		justifyContent: 'space-between'
	},
	buttonCategoryLabel: {
		fontSize: 16,
		color: '#000'
	},
	map: {
		flex: 1,
		...StyleSheet.absoluteFillObject
	},
	pin: {
		width: 50,
		height: 50
	},
	iconOk: {
		width: 24,
		height: 24,
		marginLeft: 16
	}
});
