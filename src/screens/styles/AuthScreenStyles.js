import { StyleSheet } from 'react-native';

export default StyleSheet.create({
	container: {
		backgroundColor: '#003366',
		flex: 1
	},
	loading: {
		flex: 1,
		alignItems: 'center',
		flexDirection: 'column',
		justifyContent: 'center'
	},
	wrapper: {
		paddingBottom: 16
	},
	loadingText: {
		fontWeight: 'bold',
		fontSize: 21,
		color: '#fff'
	}
});
