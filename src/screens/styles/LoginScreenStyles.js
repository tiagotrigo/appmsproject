import { 
	StyleSheet, 
	Dimensions 
} from 'react-native';

import { 
	metrics 
} from '../../themes';

export default StyleSheet.create({
	container: {
		backgroundColor: '#003366',
		flex: 1
	},
	facebook: {
		width: metrics.screenWidth,
		height: metrics.screenHeight,
		alignItems: 'center',
		flexDirection: 'row',
		justifyContent: 'center'
	},
	iconButtonFacebook: {
		width: metrics.screenWidth - 72,
		height: 68
	}
});
