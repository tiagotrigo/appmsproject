import React, { Component } from 'react';
import {
	View,
	Image,
	TouchableOpacity,
	Text,
	Modal,
	AsyncStorage,
	ActivityIndicator
} from 'react-native';
// Styles
import styles from './styles/ProfileScreenStyles';
// Themes
import { images } from '../themes';
// Service
import FacebookService from '../services/FacebookService';

class ProfileScreen extends Component {
	
	static navigationOptions = ({ navigation }) => ({
		headerLeft: (
			<TouchableOpacity
				style={styles.navBarContainerLeft}
				onPress={() => navigation.navigate('ListScreenTab')}
			>
				<Image
					source={images.iconListHeader}
					style={styles.iconNavBar}
					resizeMode={'contain'}
				/>
			</TouchableOpacity>
		),
		headerRight: (
			<TouchableOpacity 
				style={styles.navBarContainerRight} 
				disabled
			>
				<Image
					source={images.iconFilter}
					style={styles.iconNavBar}
					resizeMode={'contain'}
				/>
			</TouchableOpacity>
		),
		tabBarIcon: ({ focused }) => {
			if (focused) {
				return (
					<Image
						source={images.iconProfileActive}					
						style={styles.iconNavBar}
						resizeMode={'contain'}
					/>
				);
			} else {
				return (
					<Image
						source={images.iconProfileInactive}
						style={styles.iconNavBar}
						resizeMode={'contain'}
					/>
				);
			}
		},
		tabBarLabel: ({ focused }) => (
			<View style={styles.navBarContainerTitle}>
				<Text
					style={[
						styles.navBarTitle,
						{ color: focused ? '#3F51B5' : '#404040' }
					]}
				>
					{'Perfil'}
				</Text>
			</View>
		)
	});

	constructor(props) {
		super(props);

		this.state = {
			data: null,
			modalVisible: false
		};
	}

	componentDidMount() {
		this.getFbJson();
	}

	async getFbJson() {
		const fbProfile = await AsyncStorage.getItem('fb');
	
		this.setState({
			data: JSON.parse(fbProfile)
		});
	}

	_handleClearStorage = () => {
		const {
			navigation
		} = this.props;

		this.setState({ modalVisible: false });
		// Limpa os dados de login
		AsyncStorage.clear();
		// Deslogando do Facebook
		FacebookService.Logout();
		// Redirecionar o usuário para um novo login
		navigation.navigate('Login');
	};

	renderModalLogout = () => {
		const { 
			modalVisible 
		} = this.state;

		return (
			<Modal
	          animationType="slide"
	          transparent={true}
	          visible={modalVisible}
	          onRequestClose={() => {}}
	        >
	        	<TouchableOpacity
	        		style={styles.fbModalOverlay}
	        		onPress={() => this.setState({ modalVisible: false })}
	        	/>
	        	<View style={styles.fbModalLogout}>
	        		<View style={styles.fbModalHeader}>
	        			<Text style={styles.fbModalHeaderText}>{'Sair'}</Text>
	        		</View>
	        		<View style={styles.fbModalBody}>
	        			<Text style={styles.fbModalBodyText}>{'Deseja realmente sair ?'}</Text>
	        		</View>
	        		<View style={styles.fbModalFooter}>
	        			<TouchableOpacity
			        		style={[
			        			styles.fbModalButton,
			        			styles.fbModalButtonCancel
			        		]}
			        		onPress={() => this.setState({ modalVisible: false })}
			        	>	
			        		<Text style={[
			        			styles.fbModalButtonLabel, 
			        			styles.fbModalButtonLabelCancel
		        			]}
			        		>
			        			{'Não'}
			        		</Text>
			        	</TouchableOpacity>

			        	<TouchableOpacity
			        		style={[
			        			styles.fbModalButton,
			        			styles.fbModalButtonExit
			        		]}
			        		onPress={() => this._handleClearStorage()}
			        	>	
			        		<Text style={[
			        			styles.fbModalButtonLabel, 
			        			styles.fbModalButtonLabelExit
		        			]}
			        		>
			        			{'Sim'}
			        		</Text>
			        	</TouchableOpacity>
	        		</View>
	        	</View>
	        </Modal>
		)
	};

	render() {
		const { 
			data 
		} = this.state;
		
		if (!data) {
			return (
				<View style={styles.loading}>
					<ActivityIndicator />
				</View>
			)
		}

		const {
			name,
			picture
		} = data;

		const fbName = name;
		const fbPhoto = picture.data.url;
		
		return (
			<View style={styles.container}>
				<View style={styles.fbProfile}>
					<TouchableOpacity 
						style={styles.fbPhotoButton}
						onPress={() => this.setState({ modalVisible: true })}
					>
						<Image
							style={styles.fbPhoto}
							source={fbPhoto ? { uri: fbPhoto } : images.iconPlaceHolder}
							resizeMode={'cover'}
						/>
					</TouchableOpacity>
					
					<Text 
						ellipsizeMode={'tail'}
						numberOfLines={2}
						style={styles.fbName}
					>
						{fbName}
					</Text>
				</View>
				{this.renderModalLogout()}
			</View>
		);
	}
}

export default ProfileScreen;
