import React, { Component } from 'react';
import {
	ActivityIndicator, 
	AsyncStorage, 
	StatusBar, 
	View ,
	Text
} from 'react-native';
// Facebook
import { AccessToken } from 'react-native-fbsdk';
// Styles
import styles from './styles/AuthScreenStyles';

class AuthScreen extends Component {
	
	static navigationOptions = {
		header: null
	};

	constructor(props) {
		super(props);
	}

	isAuth() {
		return new Promise((resolve, reject) => {
			AccessToken.getCurrentAccessToken().then((data) => {
				if (data) {
					if (data.accessToken) {
						resolve(true);
					} else {
						resolve(false);
					}
				} else {
					resolve(false);
				}
			}).catch(err => reject(err));
		});
	}

	componentWillMount() {
		const {
			navigation
		} = this.props;

		this.isAuth().then((res) => {
			navigation.navigate(res ? 'TabRoot' : 'Login');
		}).catch((error) => {
			console.log('Error: ', error);
		});
	}

	render() {
		return (
			<View style={styles.container}>
				<StatusBar barStyle="default" />
				<View style={styles.loading}>
					<View style={styles.wrapper}>
						<Text style={styles.loadingText}>{'Carregando...'}</Text>
					</View>
					<ActivityIndicator 
						color={'#fff'} 
					/>
				</View>
			</View>
		);
	}
}

export default AuthScreen;
