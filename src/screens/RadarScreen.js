import React, { Component } from 'react';
import {
	Text,
	View,
	Image,
	Modal,
	Alert,
	AsyncStorage,
	TouchableOpacity,
	ActivityIndicator
} from 'react-native';
// Env
import Config from 'react-native-config'
// Styles
import styles from './styles/RadarScreenStyles';
// Themes
import { images } from '../themes';
// Maps
import MapView, { Marker } from 'react-native-maps';
// Service
import MapService from '../services/MapService';
// Polyline
import Polyline from '@mapbox/polyline';

class RadarScreen extends Component {

	_keyExtractor = (item, index) => item.id;
	
	static navigationOptions = ({ navigation }) => ({
		headerLeft: (
			<TouchableOpacity
				style={styles.navBarContainerLeft}
				onPress={() => {
					navigation.navigate('ListScreenTab');
				}}
			>
				<Image
					source={images.iconListHeader}
					style={styles.iconNavBar}
					resizeMode={'contain'}
				/>
			</TouchableOpacity>
		),
		headerRight: (
			<TouchableOpacity
				style={styles.navBarContainerRight}
				onPress={() => 
					navigation.state && 
					navigation.state.params && 
					navigation.state.params.filterCategory()
				}
			>
				<Image
					source={images.iconFilter}
					style={styles.iconNavBar}
					resizeMode={'contain'}
				/>
			</TouchableOpacity>
		),
		tabBarIcon: ({ focused }) => {
			if (focused) {
				return (
					<Image
						source={images.iconRadarActive}
						style={styles.iconNavBar}
						resizeMode={'contain'}
					/>
				);
			} else {
				return (
					<Image
						source={images.iconRadarInactive}
						style={styles.iconNavBar}
						resizeMode={'contain'}
					/>
				);
			}
		},
		tabBarLabel: ({ focused }) => (
			<View style={styles.navBarContainerTitle}>
				<Text
					style={[
						styles.navBarTitle,
						{ color: focused ? '#3F51B5' : '#404040' }
					]}
				>
					{'Radar'}
				</Text>
			</View>
		)
	});

	constructor(props) {
		super(props);

		this.state = {
			region: {
				latitude: null,
				longitude: null,
				latitudeDelta: null,
				longitudeDelta: null
			},
			data: null,
			type: 'restaurant',
			places: null,
			place: null,
			coords: null,
			showPlaces: true,
			modalTraceRoute: false,
			modalFilterCategory: false,
			filterSelected: null
		};
	}

	componentDidMount() {
		const { 
			navigation 
		} = this.props;
		
		navigation.setParams({
			filterCategory: () => this.setState({ modalFilterCategory: true })
		});
		// Posição atual
		this.geoMyPosition(0.01, 0.01);
		// Facebook ( Objeto )
		this.getFbJson();
	}

	// Dados do facebook
	async getFbJson() {
		const fbProfile = await AsyncStorage.getItem('fb');
	
		this.setState({
			data: JSON.parse(fbProfile)
		});
	};

	_handleFilterSelected = (index) => {
		const { 
			filterSelected 
		} = this.state;

		if (filterSelected === index) {
			this.setState({ filterSelected: null, type: 'restaurant' });
		} else {
			this.setState({ filterSelected: index });
		}
	};

	// Atualizando os places dentro do mapa
	_handleUpdatePlaces = () => {
		setTimeout(() => {
			this.geoPlaces();
		}, 500);
	};

	_renderFilter = () => {
		const { 
			filterSelected,
			modalFilterCategory
		} = this.state;

		const collection = [
			{
				id: '0',
				name: 'Todos',
				place: 'restaurant'
			},
			{
				id: '1',
				name: 'Aeroportos',
				place: 'airport'
			},
			{
				id: '2',
				name: 'Restaurantes',
				place: 'restaurant'
			},
			{
				id: '3',
				name: 'Baladas',
				place: 'night_club'
			},
			{
				id: '4',
				name: 'Supermarcados',
				place: 'supermarket'
			},
			{
				id: '5',
				name: 'Shopping Centers',
				place: 'shopping_mall'
			}
		];

		return (
			<Modal
	          animationType="slide"
	          transparent={true}
	          visible={modalFilterCategory}
	          onRequestClose={() => {}}
	        >
	        	<View style={styles.modal}>
	        		<View style={styles.modalContent}>
	        			<View style={styles.modalHeader}>
		        			<Text style={styles.modalHeaderTitle}>{'Categorias'}</Text>
		        		</View>
	        			{collection.map((item, index) => (
	        				<TouchableOpacity
	        					key={item.id}
	        					style={styles.buttonCategory}
        						onPress={() => {
        							// Atualizando o tipo do place
        							this.setState({ 
        								type: item.place,
        								coords: null,
        								modalFilterCategory: false 
        							});
        							// Controlando o active da categoria
        							this._handleFilterSelected(index);
        							// Atualizando place
        							this._handleUpdatePlaces();
        						}}
        					>
        						<Text style={styles.buttonCategoryLabel}>{item.name}</Text>
        						{
        							filterSelected === index
        							? (
        								<Image
        									source={images.iconOk}
        									style={styles.iconOk}
        									resizeMode={'contain'}
        								/>
        							)
        							: null
        						}
        					</TouchableOpacity>
	        			))}
	        			<TouchableOpacity
	        				style={styles.buttonLink} 
	        				onPress={() => {
        						// Fechando o modal
	        					this.setState({ 
	        						modalFilterCategory: false, 
	        						coords: null 
	        					});
	        				}}
	        			>
	        				<Text style={styles.buttonLinkLabel}>{'Cancelar'.toUpperCase()}</Text>
	        			</TouchableOpacity>
	        		</View>
	        	</View>
			</Modal>
		)
	};

	// Places dentro da minha área
	geoPlaces = () => {
		const { 
			region, 
			type 
		} = this.state;

		const {
			latitude,
			longitude
		} = region;

		const icon = this._renderPin(type);
		
		const url = MapService.geoUrl(
			latitude,
			longitude,
			500,
			type,
			Config.API_KEY_PLACES
		);

		fetch(url)
			.then((data) => data.json())
			.then((res) => {
				const collection = [];

				if (res.results.length > 0) {
					res.results.map((item, index) => {
						const {
							lat,
							lng
						} = item.geometry.location;

						collection.push(
							<Marker
								key={index}
								onPress={() => {
									this.setState({
										place: item,
										modalTraceRoute: true
									});
								}}
								coordinate={{
									latitude: lat,
									longitude: lng
								}}
							>
								<View>
									<Image
										source={icon}
										style={styles.pin}
										resizeMode={'contain'}
									/>
								</View>
							</Marker>
						);
					});

					this.setState({ places: collection });
				} else {
					this._handleAlertCancel('Filtro', 'Sem resultados, por favor escolha outra categoria.', () => {
						this.setState({ modalFilterCategory: true });
					});
					this.setState({ places: [] });
				}

			});
	};

	// Sua posição atual
	geoMyPosition = (latitudeDelta, longitudeDelta) => {
		navigator.geolocation.getCurrentPosition((pos) => {
				const region = {
					latitude: pos.coords.latitude,
					longitude: pos.coords.longitude,
					latitudeDelta,
					longitudeDelta
				};
				this.setState({ region: region });
				// Carregando places de acordo com sua posição atual
				this.geoPlaces();
			},
			error => {
				alert(error.message);
			},
			{ 
				enableHighAccuracy: true, 
				timeout: 20000 
			}
		);

		this.computed = navigator.geolocation.watchPosition((pos) => {
			const update = {
				latitude: pos.coords.latitude,
				longitude: pos.coords.longitude,
				latitudeDelta,
				longitudeDelta
			};

			this.setState({ region: update });
		});
	};

	geoTrackRoute = async (startLocation, endLocation) => {
		const url = MapService.geoDirection(startLocation, endLocation, Config.API_KEY_DIRECTIONS);

		fetch(url)
			.then(async (data) => data.json())
			.then((res) => {
				let point = Polyline.decode(res.routes[0].overview_polyline.points);
				let coords = point.map((p, index) => {
					return {
						latitude: p[0],
						longitude: p[1]
					};
				});
				this.setState({ coords });
			});
	};

	// Modal para traçar rotas
	_handleTraceRoute = () => {
		const {
			place,
			region,
			modalTraceRoute
		} = this.state;

		const {
			latitude,
			longitude
		} = region;

		const {
			lat,
			lng
		} = place.geometry.location;

		const startLocation = `${latitude},${longitude}`;
		const endLocation = `${lat},${lng}`;

		return (
			<Modal
	          animationType="slide"
	          transparent={true}
	          visible={modalTraceRoute}
	          onRequestClose={() => {}}
	        >
	        	<View style={styles.modal}>
	        		<View style={styles.modalContent}>
	        			<Text
	        				style={styles.routeModalName}
	        				numberOfLines={1}
							ellipsizeMode={'tail'}
	        			>
	        				{place.name}
	        			</Text>
	        			<View style={styles.routeModalVicinityWrapper}>
		        			<Text
		        				style={styles.routeModalVicinity}
		        				numberOfLines={2}
								ellipsizeMode={'tail'}
		        			>
		        				{place.vicinity}
		        			</Text>
	        			</View>
	        			<TouchableOpacity
	        				style={styles.button} 
	        				onPress={() => {
	        					this.geoTrackRoute(startLocation, endLocation);
	        					this.setState({ modalTraceRoute: false })
	        				}}
	        			>
	        				<Text style={styles.buttonLabel}>{'Traçar rota'.toUpperCase()}</Text>
	        			</TouchableOpacity>
	        		</View>
	        	</View>
			</Modal>
		);

	};

	// Custom pin
	_renderPin = (type) => {

		let pin;

		switch(type) {
			case 'airport':
				pin = images.iconPlaceAirport;
				break;
			case 'restaurant':
				pin = images.iconPlaceRestaurant;
				break;
			case 'night_club':
				pin = images.iconPlaceParty;
				break;
			case 'supermarket':
				pin = images.iconPlaceMarket;
				break;
			case 'shopping_mall':
				pin = images.iconPlaceShopping;
				break; 
			default:
				pin = images.iconPlaceRestaurant;
				break;
		}

		return pin;
	};

	// Toggle para habilitar e dasbilitar os places ao meu redor
	_handleTogglePlaces = () => {
		const {
			showPlaces
		} = this.state;

		this.setState({ 
			showPlaces: !showPlaces,
			coords: null
		});
	};

	// Alert do tipo cancelar
	_handleAlertCancel = (title, description, func) => {
		Alert.alert(
		  title,
		  description,
		  [
		    {
		      text: 'Cancelar',
		      onPress: () => func(),
		      style: 'cancel',
		    },
		  ],
		  {	cancelable: false },
		);
	};

	render() {
		const { 
			data, 
			region, 
			places,
			place,
			coords,
			showPlaces,
			modalFilterCategory
		} = this.state;

		const {
			latitude,
			longitude
		} = region;
		
		if (!data && latitude === null) {
			return (
				<View style={styles.loading}>
					<ActivityIndicator />
				</View>
			);
		}

		const fbPhoto = data && data.picture && data.picture.data && data.picture.data.url;
		
		return (
			<View style={styles.container}>
				{
					latitude != null 
					? (
						<MapView
							ref={(map) => this.map = map}
							style={styles.map}
							provider="google"
							initialRegion={region}
							onMapReady={this.onMapReady}
							showsMyLocationButton={false}
							onRegionChange={this.onRegionChange}
							onRegionChangeComplete={this.onRegionChangeComplete}
						>
							<Marker
								onPress={() => this._handleTogglePlaces()}
								coordinate={{
									latitude,
									longitude
								}}
							>
								<View style={styles.fbProfile}>
									<Image
										style={styles.fbPhoto}
										source={fbPhoto ? { uri: fbPhoto } : images.iconPlaceHolder}
										resizeMode={'contain'}
									/>
								</View>
							</Marker>
							{
								coords
								? (
									<MapView.Polyline
										coordinates={coords}
										strokeWidth={6}
										strokeColor={"#3F51B5"}
										strokeColors={[
											'#7F0000',
											'#00000000',
											'#B24112',
											'#E5845C',
											'#238C23',
											'#7F0000'
										]}
									/>
								) : null
							}
							{
								// Mostrar places dentro da minha área
								showPlaces
								? (
									<View>
										{places}
									</View>
								)
								: null
							}
						</MapView>
					) 
					: null
				}
				{	
					// Modal do place clicado
					place
					? this._handleTraceRoute()
					: null
				}
				{
					// Modal listando as categorias
					modalFilterCategory
					? this._renderFilter()
					: null
				}
			</View>
		);
	}
}

export default RadarScreen;
