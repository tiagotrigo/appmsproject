import React, { Component } from 'react';
import {
	View,
	Image,
	TouchableOpacity,
	Text,
	FlatList,
	AsyncStorage,
	Alert,
	ActivityIndicator
} from 'react-native';
// Env
import Config from 'react-native-config'
// Styles
import styles from './styles/ListScreenStyles';
// Themes
import { images } from '../themes';
// Service
import MapService from '../services/MapService';

class ListScreen extends Component {

	 _keyExtractor = (item, index) => item.id;
	
	static navigationOptions = ({ navigation }) => ({
		headerLeft: (
			<TouchableOpacity 
				style={styles.navBarContainerLeft} 
				disabled
			>
				<Image
					source={images.iconListHeader}
					style={styles.iconNavBar}
					resizeMode={'contain'}
				/>
			</TouchableOpacity>
		),
		headerRight: (
			<TouchableOpacity 
				style={styles.navBarContainerRight} 
				disabled
			>
				<Image
					source={images.iconFilter}
					style={styles.iconNavBar}
					resizeMode={'contain'}
				/>
			</TouchableOpacity>
		),
		tabBarIcon: ({ focused }) => {
			if (focused) {
				return (
					<Image
						source={images.iconListActive}
						style={styles.iconNavBar}
						resizeMode={'contain'}
					/>
				);
			} else {
				return (
					<Image
						source={images.iconListInactive}
						style={styles.iconNavBar}
						resizeMode={'contain'}
					/>
				);
			}
		},
		tabBarLabel: ({ focused }) => (
			<View style={styles.navBarContainerTitle}>
				<Text
					style={[
						styles.navBarTitle,
						{ color: focused ? '#3F51B5' : '#404040' }
					]}
				>
					{'Lista'}
				</Text>
			</View>
		)
	});

	constructor(props) {
		super(props);

		this.state = {
			region: {
				latitude: null,
				longitude: null,
				latitudeDelta: null,
				longitudeDelta: null
			},
			type: 'restaurant',
			stories: [],
      		isFetching: false
		};
	}

	componentDidMount() {
		// latDelta e longDelta
		this.geoMyPosition(0.01, 0.01);
		this.fetchData();
	}

	onRefresh() {
		this.setState({ isFetching: true }, () => { 
			this.fetchData();
		});
	}

	geoMyPosition = (latitudeDelta, longitudeDelta) => {
		navigator.geolocation.getCurrentPosition((pos) => {
				const region = {
					latitude: pos.coords.latitude,
					longitude: pos.coords.longitude,
					latitudeDelta,
					longitudeDelta
				};

				this.setState({ region });

				this.fetchData();

			}, (error) => {
				alert(error.message);
			},
			{ 
				enableHighAccuracy: true, 
				timeout: 20000 
			}
		);

		this.computed = navigator.geolocation.watchPosition((pos) => {
			const update = {
				latitude: pos.coords.latitude,
				longitude: pos.coords.longitude,
				latitudeDelta,
				longitudeDelta
			};

			this.setState({ region: update });
		});
	};

	fetchData = () => {
		const { 
			region, 
			type 
		} = this.state;

		const url = MapService.geoUrl(
			region.latitude, 
			region.longitude, 
			500, 
			type, 
			Config.API_KEY_PLACES
		);

		fetch(url)
			.then((data) => data.json())
			.then((res) => {
				this.setState({ 
					stories: res, 
					isFetching: false 
				});
			}
		);
	};

	_renderItem = ({ item, index }) => {

		const {
			icon,
			name,
			vicinity,
			formatted_phone_number
		} = item;

		const {
			stories
		} = this.state;

		const pIcon = icon;
		const pName = name;
		const pAddress = vicinity;
		const pPhone = formatted_phone_number;

		return (
			<View 
				style={[
					styles.pItem, 
					((stories.length - 1) === index) ? { borderWidth: 0 } : null
				]}
			>
				<View style={styles.pIcon}>
					<Image
						source={{ uri: pIcon }}
						style={styles.pThumb}
						resizeMode={'contain'}
					/>
				</View>
				<View style={styles.pGroup}>
					<Text 
						style={styles.pName}
						numberOfLines={1}
						ellipsizeMode={'tail'}
					>
						{pName}
					</Text>
					<View style={styles.pAddressWrapper}>
						<Text
							style={styles.pAddress}
							numberOfLines={2}
							ellipsizeMode={'tail'}
						>
							{pAddress}
						</Text>
					</View>
					{
						pPhone 
						? (
							<Text
								style={styles.pPhone}
								numberOfLines={1}
								ellipsizeMode={'tail'}
							>
								{pPhone}
							</Text>
						) : (
							<Text
								style={styles.pPhone}
								numberOfLines={1}
								ellipsizeMode={'tail'}
							>
								{'Não informado'}
							</Text>
						)
					}
				</View>
			</View>
		);
	};

	render() {
		const { 
			stories,
			isFetching 
		} = this.state;
		
		if (!stories) {
			return (
				<View style={styles.loading}>
					<ActivityIndicator />
				</View>
			)
		}
		
		return (
			<View style={styles.container}>

				{
					stories.results 
					? (
						<FlatList
							data={stories.results}
							extraData={this.state}
							renderItem={this._renderItem}
							keyExtractor={this._keyExtractor}
						/>
					) : null
				}
			</View>
		);
	}
}

export default ListScreen;
