import React, { Component } from 'react';
// React
import { 
	View,
	Image,
	TouchableOpacity
} from 'react-native';
// Styles
import styles from './styles/LoginScreenStyles';
// Themes
import { images } from '../themes';
// Facebook
import FacebookService from '../services/FacebookService';

class LoginScreen extends Component {
	
	static navigationOptions = {
		header: null
	};

	constructor(props) {
		super(props);

		this.state = {};
	}

	render() {
		return (
			<View style={styles.container}>
				<View style={styles.facebook}>
					<TouchableOpacity
						style={styles.facebookActionButton}
						onPress={() => FacebookService.Login()}
					>
						<Image
							source={images.iconButtonFacebook}
							style={styles.iconButtonFacebook}
							resizeMode={'contain'}
						/>
					</TouchableOpacity>
				</View>
			</View>
		);
	}
}

export default LoginScreen;
