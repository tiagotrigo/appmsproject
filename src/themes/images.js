// leave off @2x/@3x
const images = {
	iconFilter: require('../../assets/action_icon_filter.png'),
	iconButtonFacebook: require('../../assets/bt_login.png'),
	iconListHeader: require('../../assets/list_header.png'),
	iconListActive: require('../../assets/list_active.png'),
	iconListInactive: require('../../assets/list_inactive.png'),
	iconPlaceAirport: require('../../assets/place_airport.png'),
	iconPlaceMarket: require('../../assets/place_market.png'),
	iconPlaceParty: require('../../assets/place_party.png'),
	iconPlaceRestaurant: require('../../assets/place_restaurant.png'),
	iconPlaceShopping: require('../../assets/place_shopping.png'),
	iconProfileActive: require('../../assets/profile_active.png'),
	iconProfileInactive: require('../../assets/profile_inactive.png'),
	iconRadarActive: require('../../assets/radar_active.png'),
	iconRadarInactive: require('../../assets/radar_inactive.png'),
	iconOk: require('../../assets/icon_ok.png'),
	iconPlaceHolder: require('../../assets/icon_placeholder.png')
};

export default images;
