// React
import React from 'react';

geoUrl = (lat, long, rad, typ, api) => {
	return `https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=${lat},${long}&radius=${rad}&types=${typ}&key=${api}`;
};

geoDirection = (startLocation, endLocation, api) => {
	return `https://maps.googleapis.com/maps/api/directions/json?origin=${startLocation}&destination=${endLocation}&key=${api}`;
}

export default {
	geoUrl,
	geoDirection
};
