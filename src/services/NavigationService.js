import { NavigationActions } from 'react-navigation';

let _navigator;

setTopLevelNavigator = (navigatorRef) => {
	_navigator = navigatorRef;
};

navigate = (routeName, params, action) => {
	_navigator.dispatch(
		NavigationActions.navigate({
			type: NavigationActions.NAVIGATE,
			routeName,
			params,
			action
		})
	);
};

export default {
	navigate,
	setTopLevelNavigator
};
