// React
import React from 'react';
// Async
import { AsyncStorage } from 'react-native';
import NavigationService from './NavigationService';
// Facebook
import {
	LoginManager,
	AccessToken,
	GraphRequest,
	GraphRequestManager
} from 'react-native-fbsdk';

Login = () => {
	LoginManager.logInWithReadPermissions([
		'public_profile', 
		'email'
	]).then((result) => {
        if (result.isCancelled) {
          console.log('Login cancelled');
        } else {
        	// Version android 4.4
			AccessToken.getCurrentAccessToken().then((data) => {
				let accessToken = data.accessToken;

				const responseInfoCallback = (error, result) => {
					if (error) {
						console.log(error);
					} else {
						this._handleFacebookSave(result);
					}
				};

				const infoRequest = new GraphRequest('/me', {
					accessToken,
					parameters: {
						fields: {
							string: 'id, email, name, picture.type(large)'
						}
					}
				}, responseInfoCallback);
				
				new GraphRequestManager()
					.addRequest(infoRequest)
					.start();
			});
       	}
	}, (error) => {
		console.log(error);
	});
};

Logout = () => {
	LoginManager.logOut();
};

_handleFacebookSave = async (data) => {
	// Salvando os dados do facebook no Storage
	await AsyncStorage.setItem('fb', JSON.stringify(data));
	// Redirecionando o usuário para a tela de loading
	NavigationService.navigate('Loading');
};

export default {
	Login,
	Logout,
	_handleFacebookSave
};
